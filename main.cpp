#include "Engine.hpp"
#include <SFML/Audio.hpp>
#include "audio/audio-analyzer/AudioAnalyzer.hpp"
#include "scene/predefined_scenes.hpp"



/**
 * Load OpenGL and its modules, creates window draws clear the window and update inputs in main loop
 */
int main()
{
    AudioAnalyzer *audioAnalyzer = nullptr;

    if(audioAnalyzer) {
        const char *song = "loopback-capture.wav";
        std::ifstream f(song);
        if(!f.good()) {
            std::cout << "ERROR::MUSIC_FILE NOT FOUND" << std::endl;
            return -1;
        }

        sf::SoundBuffer buffer;
        buffer.loadFromFile(song);
        sf::Sound sound = sf::Sound(buffer);
        audioAnalyzer = new AudioAnalyzer(sound, buffer, false);
    }

    Engine engine(1920, 1080, "Fractales", audioAnalyzer, scenePlanet);

    if(audioAnalyzer)
        engine.getAudioVisualizer()->sound.play();

    while(!glfwWindowShouldClose(engine.getWindow()))
    {
        engine.update();
        engine.render();
    }

    delete audioAnalyzer;
    return 0;
}