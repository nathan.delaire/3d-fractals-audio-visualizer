//
// Created by natha on 07/04/2021.
//

#pragma once

#include "../shaders/Shader.hpp"

/**
 * @class : Material sets all the lighting properties of an object
 * It's ambient diffuse and specular lighting
 */
class Material {
public:
    Material(const glm::vec3 &ambient,
             const glm::vec3 &diffuse,
             const glm::vec3 &specular);

    ~Material();

    //Send material to shader
    void sendToShader(Shader &program);

private:
    //RGBs
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
};



