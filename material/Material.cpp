//
// Created by natha on 07/04/2021.
//

#include "Material.hpp"

Material::Material(const glm::vec3 &ambient, const glm::vec3 &diffuse, const glm::vec3 &specular) : ambient(ambient), diffuse(diffuse), specular(specular){}

Material::~Material() {}

void Material::sendToShader(Shader &program) {
    program.setVec3f(ambient, "material.ambient");
    program.setVec3f(diffuse, "material.diffuse");
    program.setVec3f(specular, "material.specular");
}
