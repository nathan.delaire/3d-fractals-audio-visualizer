//
// Created by natha on 07/04/2021.
//

#ifndef RAYMARCHING_VERTEX_HPP
#define RAYMARCHING_VERTEX_HPP


#include <glm/glm.hpp>

//A simple point
struct Vertex{
    glm::vec3 position;
};


#endif //RAYMARCHING_VERTEX_HPP
