//
// Created by natha on 16/04/2021.
//

#ifndef RAYMARCHING_SCENE_HPP
#define RAYMARCHING_SCENE_HPP

#include <glm/vec3.hpp>
#include <vector>

enum OBJ_TYPE{
    MANDELBULB = 0,
    MANDELBOX,
    PLANETS,
    GYROID,
    SPONGE
};

static std::vector<const char*> scenes_list = {"Mandelbulb",
                                               "Mandelbox",
                                               "Planets",
                                               "Gyroid",
                                               "Sponge"};

class Scene {
public:
    Scene(OBJ_TYPE object, const glm::vec3 &color, const glm::vec3 &position, float maxRaySteps, float minDistance,
          float maxDistance, bool renderGlow, float glowLevel, bool renderAo, bool renderNormals, bool animate, bool distordedReality);

    const glm::vec3 &getPosition() const;

    bool isDistordedReality() const;

    glm::vec3 color;
    OBJ_TYPE object;
    glm::vec3 position;
    float MAX_RAY_STEPS;
    float MIN_DISTANCE;
    float MAX_DISTANCE;

    bool renderGlow;
    float glowLevel;

    bool renderAO;
    bool renderNormals;

    bool animate;
    bool distordedReality;
};




#endif //RAYMARCHING_SCENE_HPP
