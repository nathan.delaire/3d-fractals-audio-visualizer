//
// Created by natha on 16/04/2021.
//

#include "Scene.hpp"

Scene::Scene(OBJ_TYPE object, const glm::vec3 &color, const glm::vec3 &position, float maxRaySteps, float minDistance,
             float maxDistance, bool renderGlow, float glowLevel, bool renderAo, bool renderNormals, bool animate, bool distordedReality) : object(object), color(color),
                                                                                      position(position),
                                                                                      MAX_RAY_STEPS(maxRaySteps),
                                                                                      MIN_DISTANCE(minDistance),
                                                                                      MAX_DISTANCE(maxDistance),
                                                                                      renderGlow(renderGlow),
                                                                                      glowLevel(glowLevel),
                                                                                      renderAO(renderAo),
                                                                                      renderNormals(renderNormals),
                                                                                      animate(animate),
                                                                                      distordedReality(distordedReality){}
const glm::vec3 &Scene::getPosition() const {
    return position;
}

bool Scene::isDistordedReality() const {
    return distordedReality;
}
