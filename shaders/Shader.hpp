//
// Created by natha on 02/04/2021.
//
#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader{
public:
    /**
     * @param vertexFile : vertex shader we want to link
     * @param fragmentFile : fragment shader we want to link
     * @param geometryFile : possible geometry shader we want to link
     * Our shader program constructor
     */
    Shader(const char* vertexFile,
           const char* fragmentFile,
           const char* geometryFile = nullptr);

    /**
     * Simple destructor
     * Destroy our set by this->id
     */
    ~Shader();


    /**
     * glUseProgram(this->id)
     */
    void use() const;

    /**
     * glUseProgram(0)
     */
     void unuse() const;


     /**
      * @param vect : send vects of float (f) to "name" var in shader program
      */
     void set1f(GLfloat vect, const GLchar *name) const;
     void setVec3f(glm::fvec3 vect, const GLchar *name) const;

    /**
      * @param mat : send matrices to "name" var in shader program
      */
    void setMatrix4fx(glm::mat4 mat, const GLchar *name, GLboolean transpose = GL_FALSE) const;

     /**
      * @param value : send one int (i) to "name" var in shader program
      */
     void set1i(GLint value, const GLchar* name) const;

    /**
     * @param value : send one boolean (i) to "name" var in shader program
     */
    void set1b(GLboolean value, const GLchar* name) const;

private:
    /**
     * @param filename : GLSL Shader file
     * @return : The shader program in a std::string
     */
    std::string loadShaderSource(const char* filename);

    /**
     * @param type :  GL_VERTEX_SHADER | GL_FRAGMENT_SHADER | GL_GEOMETRY_SHADER
     * @param filename : GLSL Shader file
     * @return The id of the program shader loaded
     */
    GLuint loadShader(GLenum type, const char* filename);

    /**
     * @param vertexShader : program that can be linked to our program
     * @param geometryShader : ''
     * @param fragmentShader : ''
     * All not parameters can be existing in the function
     */
    void linkProgram(GLuint vertexShader,
                     GLuint geometryShader,
                     GLuint fragmentShader);

    //Our shader program id
    GLuint id;
};