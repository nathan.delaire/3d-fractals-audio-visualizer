//
// Created by natha on 08/04/2021.
//

#ifndef RAYMARCHING_ENGINE_HPP
#define RAYMARCHING_ENGINE_HPP

#include "libs.h"

class Engine {
public:
    //Constructor / Destructor
    Engine(int wWidth, int wHeight, const char *title, AudioAnalyzer *audioVisualizer, Scene* scene = nullptr);

    virtual ~Engine();

    void update();
    void render();

    GLFWwindow *getWindow() const;

    AudioAnalyzer *getAudioVisualizer() const;


private:

    /**
     * Init GLFW lib
     */
    void initGLFW();

    /**
    * @param window
    * @param fbW
    * @param fbH
    * Update frame when window is resized
     */
    static void framebuffer_resize_callback(GLFWwindow *window, int fbW, int fbH);

    /**
    * @param widht
    * @param height
    * @return A window configured with width and height
    */
    void initWindow();

    /**
     * Init GLEW lib
     */
    void initGLEW();

    /**
     * Init ImGUI
     */
     void initImGUI();

    /**
     * Set options for OpenGL
     */
    void initOpenGLOptions();
    /**
     * Init GameObjects
     */
    void initMatrices();
    void initShaders();
    void initMaterials();
    void initFrame();

    /**
     * If no scene is loaded, we load manually the object in the initObjects function
     * If the user decided to load a scene, we get the json file values in the initScene function
     */
    void initObjects();
    void initScene();

    void initLights();

    /**
     * If we want to have a audio synched with animations, we need to initialize it
     */
     void initMusic();

    /**
     * Send Objects needed to GPU shaders
     */
    void initUniforms();

    /**
     * Update objects in GPU shaders for each frame
     */
    void updateUniforms();

    /**
    * @param window : Our current activated window
    * This function is used to move around the world using ESFX keys
    */
    void updateKeyboardInput();
    /**
     * Checks cursor movement in the app and update variables
     */
    void updateMouseInput();
    /**
     * Useful for framerate calculation
     */
    void updateDeltaTime();

    /**
     * Update our frame in front of the camera depending on the position of the camera
     */
     void updateFrame();

     /**
      * Update scene if necessary
      */
      void updateScene();

     /**
      * Update objects positions, if we want to animate then
      */
      void updateObjects();

      /**
       * Update Low, mid and high values of a audio if we putted one in parameter
       */
       void updateMusic(bool capture);

       /**
        * Update fov, minRayDust and maxRaySteps if we want to distord reality
        * according to how far we are from the fractal
        */
       void updateReality();

       /**
        * Update user interface
        */
       void updateImGui();

    //window
    GLFWwindow *window;
    const int W_WIDTH;
    const int W_HEIGHT;
    int fbwidth;
    int fbheight;
    const char* title;

    //Delta time (movement independant from framerate)
    float dt;
    float curTime;
    float lastTime;

    //Mouse variables
    double lastMouseX;
    double lastMouseY;
    double mouseX;
    double mouseY;
    double mouseOffsetX;
    double mouseOffsetY;
    //For first call
    bool firstMouse;

    //Camera and view matrices
    glm::mat4 ViewMatrix;

    glm::vec3 camPosition;
    glm::vec3 camUp;
    glm::vec3 camFront;
    float fov;
    float nearPlane;
    float farPlane;
    glm::mat4 ProjectionMatrix;

    //Material for light reflection
    Material* material;

    //Main Shader
    Shader* shader;

    //Frame from where we'll cast all the raymarch rays
    Mesh* frame;

    //Light
    glm::vec3* light;

    //Object in the scene
    std::vector<Object*> objects;

    //Camera
    Camera camera;

    //If we want to load a particular scene
    Scene *loadedScene;

    //For audio synchronization
    AudioAnalyzer *audioVisualizer;
    //For audio output synch
    AudioAnalyzer *capturedAudio;

    //For device list
    std::vector<const char *> deviceList;
    std::vector<LPCWSTR> deviceList_t;


    //If he game is in pause sate, if we want the cursor posiion to no be updated
    bool is_Pause;

    int selected_audio_output;
    float is_rotating;
    float is_moving;

    int selected_scene;
    int current_loaded_scene;

    int lowLevel = 16900000;
    int midLevel = 16900000;
};



#endif //RAYMARCHING_ENGINE_HPP
